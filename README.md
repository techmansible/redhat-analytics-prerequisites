
DLP Analytics Engine:


Analytics Prerequisites Role: redhat-analytics-Prerequisites

Following task will be performed by the playbook

1. Stop Firewall and disable from boot
2. set hostname and fully qualified name are identical 
3. Install freetds service
4. Installing the following packages apr, apr-util, perl-Switch, libtool-ltdl, unixODBC
5. Ansible pexpect module installation (its required for automate interactive mode of installation in remote machine)
6. Check the windows machine is reachable to start the installation